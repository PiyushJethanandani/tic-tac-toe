var h, c;
var a1, a2, a3, a4, a5, a6, a7, a8, a9;
var turn = 0;

function setText(id,text) {
    document.getElementById(id).innerHTML = text;
}



function show(id) {
    document.getElementById(id).style.display = 'block';
}

function hide(id) {
    document.getElementById(id).style.display = 'none';
}

document.getElementById("x").onclick = function () {
    h = "X";
    c = "O";
    show("board");
    hide("choice");
}

document.getElementById("o").onclick = function () {
    h = "O";
    c = "X";
    show("board");
    hide("choice");
}

document.getElementById("newgame").onclick =function () {
    window.location.reload();
}

document.getElementsByTagName("td").onclick =function () {
    if (turn === 0) {
        if (document.getElementsByTagName(this).innerHTML === "") {
          
            document.getElementsByTagName(this).text(h);
            checkBoxState();
            checkWinning();
            turn = 1;
            computerMove();
            checkBoxState();
            checkWinning();
        } else {
            alert("There is already a move on that square. Please pick a different square.");
        }
    }
}
