/*

Syntax of jQuery
$(selector).member
*/

//the below function is document ready function
//// it is invoked whn all DOM is loaded
//$(document).ready(function(){
//    alert("Hello");
//});

//But jQuery also has a short hand version for document ready Event,which is defined as follows
/*
$(function(){
    //body of function
});
*/

$(function(){
    //alert("Helo world);
//    $("#box").hide();
    $(".thing").fadeOut(1000);
    $("button").click(function(){
        $("#box").fadeOut(1000);
    });











/************************************SELECTORS********************************************/

    //grouping selector
    
    $("h2","h3","h4").css("border","solid 1px red");
    //id selector
    $("div#container").css("border","solid 1px red");
    //class selector
    $(".lead").css("border","dashed 1px red");
    //descendant selector
    $("div em").css("border","solid 1px red");
    //child selector
    $("div>p").css("border","solid 1px blue" );
    //Pseudo selectors
    $("li:first").css("border","solid 1px pink");
    //All even elements
    $("p:even").css("border","solid 1px pink");
    //All heading
    $(":header").css("text-align","center");
    //jQuery Contains Selector
    $("div:contains('love')").css("border","solid 1px green");




/**************************************EVENTS******************************************/
    $("#box").click(function(){
        alert("You jusr clicked me");
    });

    $("input[type='text']").blur(function(){
        if($(this).val()===""){
            $(this).css("border","solid 1px red");
            $("#box").text("Forgot to add Something");
        }
    });

    $("input[type='text']").keydown(function(){
        if($(this).val()!==""){
            $(this).css("border","solid 1px green");
            $("#box").text("Thanka Bye");
        }
    });
    
    $("#box").hover(function(){
        $(this).text("You Hovered in");
    }, function(){
            $(this).text("you hovered out");
    });
    
    
    
        /********************jqueryChaining************************/
        $(".notification-bar").delay(2000).slideDown().delay(3000).slideUp();
    
});