$(document).ready(function () {
    var h, c;
    var a1, a2, a3, a4, a5, a6, a7, a8, a9;
    var turn = 0;
    var countHumanWins = 0;
    var countAIWins = 0;

    $("#x").click(function () {
        h = "X";
        c = "O";
        $("#choice").fadeOut();
        $("#board").fadeTo("slow", 1);
    });
    $("#o").click(function () {
        h = "O";
        c = "X";
        $("#choice").fadeOut();
        $("#board").fadeTo("slow", 1);
    });

    $("#newgame").click(function () {
        location.reload();
    });

    $("td").click(function () {
        if (turn === 0) {
            if ($(this).text() === "") {
                $(this).text(h);
                checkBoxState();
                checkWinning();
                turn = 1;
                computerMove();
                checkBoxState();
                checkWinning();
            } else {
                alert("There is already a move on that square. Please pick a different square.");
            }
        }
    });

    function checkWinning() {
        // h wins
        if ((a1 === a2 && a1 === a3 && (a1 === h)) ||
            (a4 === a5 && a4 === a6 && (a4 === h)) ||
            (a7 === a8 && a7 === a9 && (a7 === h)) ||
            (a1 === a4 && a1 === a7 && (a1 === h)) ||
            (a2 === a5 && a2 === a8 && (a2 === h)) ||
            (a3 === a6 && a3 === a9 && (a3 === h)) ||
            (a1 === a5 && a1 === a9 && (a1 === h)) ||
            (a3 === a5 && a3 === a7 && (a3 === h))) {

            $("#board").fadeOut("slow");
            $("#game-over").text("You win!");
            countHumanWins++;
            $("#hscoreValue").text(countHumanWins);

            $("#game-over").fadeTo("slow", 1);
            $("#newgame").fadeIn("slow", 1);
        }
        // computer wins
        else if ((a1 === a2 && a1 === a3 && (a1 === c)) ||
            (a4 === a5 && a4 === a6 && (a4 === c)) ||
            (a7 === a8 && a7 === a9 && (a7 === c)) ||
            (a1 === a4 && a1 === a7 && (a1 === c)) ||
            (a2 === a5 && a2 === a8 && (a2 === c)) ||
            (a3 === a6 && a3 === a9 && (a3 === c)) ||
            (a1 === a5 && a1 === a9 && (a1 === c)) ||
            (a3 === a5 && a3 === a7 && (a3 === c))
        ) {
            $("#board").fadeOut("slow");
            $("#game-over").text("You Lost!");
            countAIWins++;
            $("#game-over").fadeTo("slow", 1);
            $("#newgame").fadeIn("slow", 1);
        }
        // tie
        else if (a1 && a2 && a3 && a4 && a5 && a6 && a7 && a8 && a9) {
            $("#board").fadeOut("slow");
            $("#game-over").text("Tie!");
            $("#game-over").fadeTo("slow", 1);
            $("#newgame").fadeIn("slow", 1);
        }
    }

    function checkBoxState() {
        a1 = $("#a1").html();
        a2 = $("#a2").html();
        a3 = $("#a3").html();
        a4 = $("#a4").html();
        a5 = $("#a5").html();
        a6 = $("#a6").html();
        a7 = $("#a7").html();
        a8 = $("#a8").html();
        a9 = $("#a9").html();
    }

    function computerMove() {
        // case 1: if there is a chance to win for computer
        if (a1 === "" && ((a2 === c && a3 === c) || (a4 === c && a7 === c) || (a5 === c && a9 === c))) {
            $("#a1").text(c);
            turn = 0;
        } else if (a2 === "" && ((a1 === c && a3 === c) || (a5 === c && a8 === c))) {
            $("#a2").text(c);
            turn = 0;
        } else if (a3 === "" && ((a1 === c && a2 === c) || (a6 === c && a9 === c) || (a5 === c && a7 === c))) {
            $("#a3").text(c);
            turn = 0;
        } else if (a4 === "" && ((a1 === c && a7 === c) || (a5 === c && a6 === c))) {
            $("#a4").text(c);
            turn = 0;
        } else if (a5 === "" && ((a4 === c && a6 === c) || (a1 === c && a9 === c) || (a3 === c && a7 === c))) {
            $("#a5").text(c);
            turn = 0;
        } else if (a6 === "" && ((a4 === c && a5 === c) || (a3 === c && a9 === c))) {
            $("#a6").text(c);
            turn = 0;
        } else if (a7 === "" && ((a8 === c && a9 === c) || (a1 === c && a4 === c) || (a3 === c && a5 === c))) {
            $("#a7").text(c);
            turn = 0;
        } else if (a8 === "" && ((a7 === c && a9 === c) || (a2 === c && a5 === c))) {
            $("#a8").text(c);
            turn = 0;
        } else if (a9 === "" && ((a7 === c && a8 === c) || (a3 === c && a6 === c) || (a1 === c && a5 === c))) {
            $("#a9").text(c);
            turn = 0;
        }
        // case 2: if there is a chance to block the human
        else if (a1 === "" && ((a2 === h && a3 === h) || (a4 === h && a7 === h) || (a5 === h && a9 === h))) {
            $("#a1").text(c);
            turn = 0;
        } else if (a2 === "" && ((a1 === h && a3 === h) || (a5 === h && a8 === h))) {
            $("#a2").text(c);
            turn = 0;
        } else if (a3 === "" && ((a1 === h && a2 === h) || (a6 === h && a9 === h) || (a5 === h && a7 === h))) {
            $("#a3").text(c);
            turn = 0;
        } else if (a4 === "" && ((a1 === h && a7 === h) || (a5 === h && a6 === h))) {
            $("#a4").text(c);
            turn = 0;
        } else if (a5 === "" && ((a4 === h && a6 === h) || (a1 === h && a9 === h) || (a3 === h && a7 === h))) {
            $("#a5").text(c);
            turn = 0;
        } else if (a6 === "" && ((a4 === h && a5 === h) || (a3 === h && a9 === h))) {
            $("#a6").text(c);
            turn = 0;
        } else if (a7 === "" && ((a8 === h && a9 === h) || (a1 === h && a4 === h) || (a3 === h && a5 === h))) {
            $("#a7").text(c);
            turn = 0;
        } else if (a8 === "" && ((a7 === h && a9 === h) || (a2 === h && a5 === h))) {
            $("#a8").text(c);
            turn = 0;
        } else if (a9 === "" && ((a7 === h && a8 === h) || (a3 === h && a6 === h) || (a1 === h && a5 === h))) {
            $("#a9").text(c);
            turn = 0;
        }
        // case 3: if center is empty AI should move there
        else if (a5 === "") {
            $("#a5").text(c);
            turn = 0;
        }
        // case 4: Always AI move should be the opposite of Human
        else if (a1 === "" && (a3 === h || a7 === h)) {
            $("#a1").text(c);
            turn = 0;
        } else if (a3 === "" && (a1 === h || a9 === h)) {
            $("#a3").text(c);
            turn = 0;
        } else if (a9 === "" && (a3 === h || a7 === h)) {
            $("#a9").text(c);
            turn = 0;
        } else if (a7 === "" && (a1 === h || a9 === h)) {
            $("#a7").text(c);
            turn = 0;
        }
        // case 5: corner
        else if (a1 === "") {
            $("#a1").text(c);
            turn = 0;
        } else if (a3 === "") {
            $("#a3").text(c);
            turn = 0;
        } else if (a7 === "") {
            $("#a7").text(c);
            turn = 0;
        } else if (a9 === "") {
            $("#a9").text(c);
            turn = 0;
        }
        // case 6: empty side
        else if (a2 === "") {
            $("#a2").text(c);
            turn = 0;
        } else if (a6 === "") {
            $("#a6").text(c);
            turn = 0;
        } else if (a8 === "") {
            $("#a8").text(c);
            turn = 0;
        } else if (a4 === "") {
            $("#a4").text(c);
            turn = 0;
        }
    }
});
